﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ifWithBool
{
    class Program
    {
        static void Main(string[] args)
        {
            var progIntro = "Welcome player, how many questions can you guess by typing [t] True or [f] False?";
            var q1 = "\n\nTony Stark is the founder of Microsoft.\n[t] True\n[f] False";
            var q2 = "\n\nThomas the Tank Engine is green.\n[t] True\n[f] False";
            var q3 = "\n\nNew Zealanders are hobbitses and all their houses have round doors.\n[t] True\n[f] False";
            var q4 = "\n\nIn what year did Mount Everest climb Sir Edmund Hillary?\n[t] True\n[f] False";
            var q5 = "\n\nThe Amazon rainforest is an online marketplace.\n[t] True\n[f] False";
            var score = 0;
            var correct = true;

            Console.WriteLine($"{progIntro}");

            // Question 1 start
            if (correct)
            {
                Console.WriteLine($"{q1}");

                if (Console.ReadLine() == "f")
                {
                    score++;
                    Console.WriteLine($"\n\nCorrect! Go away to next round!");
                    correct = true;
                }
                else
                {
                    Console.WriteLine($"\n\nGit gud m9, u lose to round 1! \nFinal score is an amazing {score}!\n\n");
                    correct = false;
                }
            }
            // Question 1 end

            // Question 2 start
            if (correct)
            {
                Console.WriteLine($"{q2}");

                if (Console.ReadLine() == "f")
                {
                    score++;
                    Console.WriteLine($"\n\nCorrect! Go away to next round!");
                    correct = true;
                }
                else
                {
                    Console.WriteLine($"\n\nYou lost to round 2. \nFinal score is {score}\n\n");
                    correct = false;
                }
            }
            // Question 2 end

            // Question 3 start
            if (correct)
            {
                Console.WriteLine($"{q3}");

                if (Console.ReadLine() == "f")
                {
                    score++;
                    Console.WriteLine($"\n\nCorrect! Go away to next round!");
                    correct = true;
                }
                else
                {
                    Console.WriteLine($"\n\nGood job! You lost to round 3 \nFinal score is {score}\n\n");
                    correct = false;
                }
            }
            // Question 3 end

            // Question 4 start
            if (correct)
            {
                Console.WriteLine($"{q4}");

                if (Console.ReadLine() == "f")
                {
                    score++;
                    Console.WriteLine($"\n\nCorrect! Go away to next round!");
                    correct = true;
                }
                else
                {
                    Console.WriteLine($"\n\nGreat! You lost to round 4 \nFinal score is {score}\n\n");
                    correct = false;
                }
            }
            // Question 4 end

            // Question 5 start
            if (correct)
            {
                Console.WriteLine($"{q5}");

                if (Console.ReadLine() == "f")
                {
                    score++;
                    Console.WriteLine($"\n\nCorrect! Go away to next round!");
                    correct = true;
                }
                else
                {
                    Console.WriteLine($"\n\nCongratulations, m9! You lose to last round \nFinal score is {score}\n\n");
                    correct = false;
                }
            }
            // Question 5 end

            if (correct)
            {
                Console.WriteLine("\n\nOh dear god you won, now I need to mortgage my house :(\n\n");
            }
        }
    }
}
